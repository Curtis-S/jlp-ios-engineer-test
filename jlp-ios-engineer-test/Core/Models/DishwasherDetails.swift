//
//  DishwasherDetails.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

struct DishwasherDetails:Equatable {
    let id:String
    let productInformation:String
}
