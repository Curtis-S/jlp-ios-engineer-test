//
//  Dishwasher.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

struct Dishwasher: Equatable {
    var productId: String
    var name: String
    var price: String
    var claimsText: String?
    var guaranteeText: String?
    var productInformation: String?
    var adjustableRackingInfo: String?
    var childLock: String?
    var delayWash:String?
    var dimensions: String?
    var dryingPerformance: String?
    var dryingSystem: String?
    var mainImageURL: String?
    var alternateImageUrls: [String]?
    var delicateWash: String?
}
