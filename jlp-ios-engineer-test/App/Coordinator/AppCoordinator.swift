//
//  AppCoordinator.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var completionDelegates: [CoordinatorCompletionDelegate] = []
    var isActive: Bool = false
    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        isActive = true
        let homeCoordinator = HomeCoordinator(window: window)
        startChildCoordinator(homeCoordinator)
    }


}

extension AppCoordinator: CoordinatorCompletionDelegate {}



