//
//  AlertConstants.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

enum AlertConstants {
    case title
    case message(Error)
    case ok
    
    var text: String {
        switch self {
        case .title:
            return "Error"
        case .message(let error):
            return "Reason: \(error)"
        case .ok:
            return "Ok"
        }
    }
}
