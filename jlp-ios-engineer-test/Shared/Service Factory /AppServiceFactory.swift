//
//  AppServiceFactory.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

/// To be used by coordinators for dependency injection 
class AppServiceFactory {
  private static var httpClient:HttpClient {
       let urlSession = URLSession(configuration: .default)
       return  DefaultHttpClient(session: urlSession)
    }
    
   static var dishwasherListRetriever:DishwasherListRetrievalService {
        let mapper =  DefaultDishwasherListMapper()
       return  DishwasherListRetriever(dishWasherListMapper: mapper, httpClient: httpClient)
    }
    
    static var dishwasherDetailsRetriever:DishwasherDetailRetrievalService {
        let mapper =  DefaultDishwasherDetailsMapper()
        return DishwasherDetailRetriever(dishwasherDetailsMapper: mapper, httpClient: httpClient)
    }
}
