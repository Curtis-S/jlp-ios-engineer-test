//
//  Coordinator.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

/// Coordinator component  is to be used for creation of viewControllers and dependency injection and app flows
 protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var completionDelegates: [CoordinatorCompletionDelegate] { get set }
    var isActive: Bool { get set }
    func start()
    func stop()
}

 protocol CoordinatorCompletionDelegate {
    func coordinatorDidFinish(_ coordinator: Coordinator)
}

//MARK: helper extentions
 extension Coordinator {
    /// Default implementation returns an empty array.
    var childCoordinators: [Coordinator] {
        get {
            []
        }
        set {}
    }

    func addCompletionDelegate(_ delegate: CoordinatorCompletionDelegate) {
        completionDelegates.append(delegate)
    }

    func start() {
        isActive = true
    }

    func startChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
        coordinator.start()
        coordinator.isActive = true
    }

    func stop() {
        isActive = false
        notifyCompletionDelegates()
    }

    func notifyCompletionDelegates() {
        completionDelegates.forEach {
            $0.coordinatorDidFinish(self)
        }
    }

    func removeChildCoordinator(_ coordinatorToRemove: Coordinator) {
        childCoordinators.removeAll { coordinator in
            coordinator === coordinatorToRemove
        }
    }
}

extension Coordinator where Self: CoordinatorCompletionDelegate {
    func startChildCoordinator(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
        coordinator.addCompletionDelegate(self)
        coordinator.start()
        coordinator.isActive = true
    }

    func coordinatorDidFinish(_ coordinatorToRemove: Coordinator) {
        removeChildCoordinator(coordinatorToRemove)
    }
}

