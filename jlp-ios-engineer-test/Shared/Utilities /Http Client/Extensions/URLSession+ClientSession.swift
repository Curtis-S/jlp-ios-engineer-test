//
//  URLSession+ClientSession.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

protocol ClientSession {
    func data(for request: URLRequest) async throws -> (Data, URLResponse)
}
extension URLSession:ClientSession {}
