//
//  DefaultHttpClient.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

typealias NetworkingResponse = (data: Data, response: HTTPURLResponse)

/// Http Component to be used for performing  the apps http requests
protocol HttpClient {
    
    /// performs the network request and returns a network response
    /// - Parameter request: url request
    /// - Returns: a response object that has the data or an error
    func data(for request: URLRequest) async throws -> NetworkingResponse
}
struct DefaultHttpClient:HttpClient {
    private let session:ClientSession
    
    init(session: ClientSession) {
        self.session = session
    }
    func data(for request: URLRequest) async throws -> NetworkingResponse {
        let result: (Data, URLResponse) = try await session.data(for: request)
        guard let httpResponse =  result.1 as? HTTPURLResponse else {
            throw HttpClientError.invalidResponse
        }
        return (result.0, httpResponse)
    }
}

