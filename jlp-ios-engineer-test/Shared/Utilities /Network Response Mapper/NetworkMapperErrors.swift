//
//  NetworkResponseMapperErrors.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

enum NetworkResponseMapperError: Error {
    case parsingError
    case nonTwoHundredErrorCode
}
