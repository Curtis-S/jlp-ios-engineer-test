//
//  NetworkResponseMapper.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

/// Generic mapper protocol used for mapping network data to object of choice
protocol NetworkResponseMapper {}

//MARK: - helpers
extension NetworkResponseMapper {
    func checkForTwoHundredHttpResponse(using response: HTTPURLResponse) -> Bool {
        
        return (200 ..< 300).contains(response.statusCode)
    }
    
    func parse<T: Decodable>(_ data: Data, urlResponse:  HTTPURLResponse, with decoder: JSONDecoder) throws -> T {
        guard checkForTwoHundredHttpResponse(using: urlResponse) else {
            throw NetworkResponseMapperError.nonTwoHundredErrorCode
        }
        
        guard let result = try? decoder.decode(T.self, from: data) else {
            throw NetworkResponseMapperError.parsingError
        }
        return result
    }
}

