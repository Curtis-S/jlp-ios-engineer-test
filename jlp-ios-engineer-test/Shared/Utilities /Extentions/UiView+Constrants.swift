//
//  UiView+Constrants.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import UIKit

extension UIView {
    func anchor(top: NSLayoutYAxisAnchor? = nil, paddingTop: CGFloat = 0, bottom: NSLayoutYAxisAnchor? = nil, paddingBottom: CGFloat = 0, left: NSLayoutXAxisAnchor? = nil, paddingLeft: CGFloat = 0, right: NSLayoutXAxisAnchor? = nil, paddingRight: CGFloat = 0, width: CGFloat = 0, height: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let right = right {
            rightAnchor.constraint(equalTo: right, constant: -paddingRight).isActive = true
        }
        if let left = left {
            leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
}

extension UIView {
    func constrainToSuperview(topPadding: Double = 0,
                              bottomPadding: Double = 0,
                              leftPadding: Double = 0,
                              rightPadding: Double = 0) {
        guard let superview = superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: superview.topAnchor, constant: topPadding).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -bottomPadding).isActive = true
        leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: leftPadding).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -rightPadding).isActive = true

    }

    func centerToSuperview(xPadding: Double = 0,
                           yPadding: Double = 0) {
        guard let superview = superview else { return }
        self.translatesAutoresizingMaskIntoConstraints = false
        centerXAnchor.constraint(equalTo: superview.centerXAnchor, constant: xPadding).isActive = true
        centerYAnchor.constraint(equalTo: superview.centerYAnchor, constant: yPadding).isActive = true
    }

    func setWidthAndHeightTo(width: Double, height: Double) {
        self.translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: width).isActive = true
        heightAnchor.constraint(equalToConstant: height).isActive = true
    }

}


