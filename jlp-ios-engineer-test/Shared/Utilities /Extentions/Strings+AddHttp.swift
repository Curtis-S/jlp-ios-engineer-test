//
//  Strings+AddHttp.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

extension String {
    func addHttpToString() -> String {
        return "https:" + self
    }
}
