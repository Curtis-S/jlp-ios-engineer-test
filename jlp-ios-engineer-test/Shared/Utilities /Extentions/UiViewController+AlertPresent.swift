//
//  UiViewController+AlertPresent.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

extension UIViewController {
    func presentAlertWith(message: String) {
        let alertController = UIAlertController(title: AlertConstants.title.text, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: AlertConstants.ok.text, style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
        present(alertController, animated: true)
        
    }
}

