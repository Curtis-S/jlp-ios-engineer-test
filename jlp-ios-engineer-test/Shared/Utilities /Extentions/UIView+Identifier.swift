//
//  UIView+Identifier.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

extension UIView {
    var identifierForCells: String {
        String(describing: type(of: self)) + "Cells"
    }
}

