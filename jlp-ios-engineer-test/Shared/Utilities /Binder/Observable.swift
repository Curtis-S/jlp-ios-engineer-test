//
//  Observable.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

/// An object made to be used for binding view model to the controller  to perform reactive updates
final class Observable<T> {
    typealias Listener = (T) -> Void
    private var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
