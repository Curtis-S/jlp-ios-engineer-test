//
//  CarouselView.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

final class CarouselView: UIView {
    private let scrollView = UIScrollView()
    private let pageIndicatior = UIPageControl()
    private var imagesToShow: [UIImageView] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupScrollView()
        setupPageControl()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setImagesForCarousel(imageUrls: [URL]) {
        for (index,imageUrl) in imageUrls.enumerated() {
            let imageView = UIImageView()
            imageView.isAccessibilityElement = true
            imageView.accessibilityLabel = "picture \(index + 1) of product"
            imageView.sd_setImage(with: imageUrl, completed: nil)
            imagesToShow.append(imageView)
        }
        
        setupCarouselViews()
        pageIndicatior.numberOfPages = imagesToShow.count
        pageIndicatior.currentPage = 0
    }
    
    private func setupCarouselViews() {
        for index in 0..<imagesToShow.count {
            let imageView = imagesToShow[index]
            imageView.contentMode = .scaleAspectFit
            imageView.frame = CGRect(x: frame.width * CGFloat(index),
                                     y: 0, width: frame.width,
                                     height: frame.height)
            scrollView.addSubview(imageView)
        }
    }
    
    private func setupScrollView() {
        addSubview(scrollView)
        scrollView.anchor(top: topAnchor,
                          bottom: bottomAnchor,
                          paddingBottom: 0,
                          left: leftAnchor,
                          right: rightAnchor)
        scrollView.isScrollEnabled = true
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: frame.width * CGFloat(imagesToShow.count), height: frame.height)
        scrollView.bounces = false
    }
    
    private func setupPageControl() {
        addSubview(pageIndicatior)
        
        pageIndicatior.anchor(top: scrollView.bottomAnchor)
        pageIndicatior.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        
        pageIndicatior.currentPageIndicatorTintColor = .black
        pageIndicatior.pageIndicatorTintColor = .gray
    }
    
    override func layoutIfNeeded() {
        scrollView.contentOffset = .zero
        scrollView.contentSize = CGSize(width: frame.width *  CGFloat(imagesToShow.count), height: frame.height)
        
        for i in 0..<imagesToShow.count {
            imagesToShow[i].frame = CGRect(x: frame.width * CGFloat(i), y: 0, width: frame.width, height: frame.height)
        }
        super.layoutIfNeeded()
    }
}

extension CarouselView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageIndicatior.currentPage = Int( scrollView.contentOffset.x / frame.width)
    }
}


