//
//  LoadingView.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

final class LoadingView: UIView {
    private let activitySpinner = UIActivityIndicatorView(style: .large)
    
    var isLoading = true {
        didSet {
            self.isHidden = !isLoading
        }
    }
    
    init() {
        super.init(frame: .zero)
        backgroundColor = .white.withAlphaComponent(0.7)
        addSubview(activitySpinner)
        activitySpinner.startAnimating()
        activitySpinner.centerToSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

