//
//  DishwasherApiEndpoints.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

enum DishwasherApiEndpoint{
    case list
    case detail
    
    var url:URL {
        switch self {
        case .list:
            URL(string: "https://gitlab.com/jlp-jobs/jlp-ios-engineer-test/-/raw/main/mockData/data.json?ref_type=heads")!
        case .detail:
            URL(string: "https://gitlab.com/jlp-jobs/jlp-ios-engineer-test/-/raw/main/mockData/data2.json?ref_type=heads")!
        }
    }
}
