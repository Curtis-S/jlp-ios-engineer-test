//
//  DishwasherHomeConstants.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

enum DishwasherHomeConstants: String {
    case homeTitle = "Dishwashers"
    case missingData = "N/A"
}
