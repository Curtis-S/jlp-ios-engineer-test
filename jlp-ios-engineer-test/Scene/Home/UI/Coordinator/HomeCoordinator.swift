//
//  HomeCoordinator.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import UIKit

class HomeCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var completionDelegates: [CoordinatorCompletionDelegate] = []
    var isActive: Bool = false
    
   private lazy var dishwasherListViewController:UIViewController = {
        let viewModel = DishwasherListViewModel(washerLoader: AppServiceFactory.dishwasherListRetriever, delegate: self)
        let listViewController = DishwasherListViewController(viewModel: viewModel)
        return  listViewController
    }()
    
    private var navigation: UINavigationController?
    private let window: UIWindow
    
    init(window: UIWindow
    ) {
        self.window = window
    }
    
    func start() {
        self.isActive = true
        let viewController = dishwasherListViewController
    
        let navigation = UINavigationController(rootViewController: viewController)
                    window.rootViewController = navigation
                    window.makeKeyAndVisible()
        self.navigation = navigation
      
    }
        
    private func makeDetailsViewController (using dishwasher:Dishwasher) -> UIViewController {
        let viewModel = DishwasherDetailViewModel(dishwasher: dishwasher, loader: AppServiceFactory.dishwasherDetailsRetriever)
        let detailsViewController = DishwasherDetailViewController(viewModel: viewModel)
        return detailsViewController
    }
}

extension HomeCoordinator:DishwasherListViewModelDelegate{
    func showDetailFor(item: Dishwasher) {
        guard let navigation else { return }
        navigation.pushViewController(makeDetailsViewController(using: item), animated: true)
    }
    
}

