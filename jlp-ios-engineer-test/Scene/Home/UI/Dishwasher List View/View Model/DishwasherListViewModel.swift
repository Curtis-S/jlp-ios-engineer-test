//
//  DishWasherListViewModel.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

protocol DishwasherListViewModelDelegate:AnyObject {
    func showDetailFor(item:Dishwasher)
}

final class DishwasherListViewModel {
     private var dishwashers: [Dishwasher] = []
     private var washerLoader:  DishwasherListRetrievalService
     private weak var delegate: DishwasherListViewModelDelegate?
    var stateDidChange = Observable(())
     var isLoading = false
     var errorMessageToDisplay: Error? = nil
    
    init(washerLoader:  DishwasherListRetrievalService,delegate: DishwasherListViewModelDelegate?) {
        self.washerLoader = washerLoader
        self.delegate = delegate
    }
    
    func loadDishwashers()  {
        isLoading = true
        stateDidChange.value = ()
        washerLoader.fetchDishwashers { [weak self] result in
            switch result {
            case .success(let dishwashers):
                self?.isLoading = false
                self?.dishwashers = dishwashers
                self?.stateDidChange.value = ()
            case .failure(let error):
                self?.isLoading = false
                self?.errorMessageToDisplay = error
                self?.stateDidChange.value = ()
            }
        }
    }
    
    func numberOfItems() -> Int {
        dishwashers.count
    }
    
    func numberOfSections() -> Int {
        1
    }
    
    func dataForItemAt(_ row: Int) -> DishwasherCollectionCell.ViewModel {
        DishwasherCollectionCell.ViewModel(dishwasher: dishwashers[row])
    }
    
    func didSelectItemAt(_ row: Int) {
        guard row >= 0 ,row < dishwashers.count else {
            return
        }
        let dishwasher = dishwashers[row]
        delegate?.showDetailFor(item: dishwasher)
    }
}
