//
//  DishwasherDetailCellConstants.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

enum DishwasherDetailCellConstants: String, CaseIterable {
    case adjustableRackingInformation = "Adjustable racking"
    case childLock = "Child lock"
    case delayWash = "Delay wash"
    case delicateWash = "Delicate wash"
    case dimensions = "Dimensions"
    case dryingPerformance = "Drying performance"
    case dryingSystem = "Drying system"
    
    var text: String {
        self.rawValue
    }
    
    // MARK: - Helpers
   
    func textForRowSubtitleUsing(dishwasher: Dishwasher) -> String? {
        switch self {
        case .adjustableRackingInformation:
            return dishwasher.adjustableRackingInfo
        case .childLock:
            return dishwasher.childLock
        case .delayWash:
            return dishwasher.delayWash
        case .delicateWash:
            return dishwasher.delicateWash
        case .dimensions:
            return dishwasher.dimensions
        case .dryingPerformance:
            return dishwasher.dryingPerformance
        case .dryingSystem:
            return dishwasher.dryingSystem
        }
    }
}
