//
//  DishwasherCollectionCell.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit
import SDWebImage

class DishwasherCollectionCell: UICollectionViewCell {
    private let detailLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .headline)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    private let imageView: UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    private let stackView = UIStackView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        contentView.addSubview(imageView)
        contentView.addSubview(stackView)
        contentView.layer.borderColor = UIColor.gray.cgColor
        contentView.layer.borderWidth = 1
        
        imageView.anchor(top: contentView.topAnchor, paddingTop: 24, bottom: contentView.centerYAnchor, paddingBottom: -16, left: contentView.leftAnchor, paddingLeft: 16, right: contentView.rightAnchor, paddingRight: 16)
        setupStackView()
    }
    
    private func setupStackView() {
        stackView.anchor(top: imageView.bottomAnchor, paddingTop: 8, left: contentView.leftAnchor, paddingLeft: 16, right: contentView.rightAnchor, paddingRight: 16)
        stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -16).isActive = true
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .top
        stackView.addArrangedSubview(detailLabel)
        stackView.addArrangedSubview(priceLabel)
    }
    
    
    
    func configureWith(viewModel: ViewModel) {
        if let url = URL(string: viewModel.imageUrl.addHttpToString()) {
            imageView.sd_setImage(with: url, completed: nil)
        }
        
        priceLabel.text = viewModel.price
        detailLabel.text = viewModel.name
        
        imageView.isAccessibilityElement = true
        imageView.accessibilityLabel = "Picture of \(viewModel.name)"
    }
    
}


