//
//  DishwasherCellViewModel.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

extension DishwasherCollectionCell {
    struct ViewModel {
        let imageUrl: String
        let price: String
        let name: String
        
        init(dishwasher: Dishwasher) {
            imageUrl = dishwasher.mainImageURL ?? DishwasherHomeConstants.missingData.rawValue
            price = dishwasher.price
            name = dishwasher.name
        }
    }
}
