//
//  DishwasherListViewController.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

class DishwasherListViewController: UIViewController {
    // MARK: - properties
    private let viewModel: DishwasherListViewModel
    private lazy var collectionView: UICollectionView = {
        let layout =  UICollectionViewLayout()
        
        let collectView = UICollectionView(frame: .zero,
                                           collectionViewLayout: collectionViewLayout())
        collectView.register(DishwasherCollectionCell.self,
                             forCellWithReuseIdentifier: self.view.identifierForCells)
        
        return collectView
    }()
    
    private lazy var loadingView: LoadingView = {
        let loadView = LoadingView()
        view.addSubview(loadView)
        loadView.constrainToSuperview()
        return loadView
    }()
    
    private var cellSize : CGSize {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return CGSize(width: (width/2), height: (height/2))
        case .pad:
            return CGSize(width: (width/4), height: (height/3))
        default:
            break
        }
        return CGSize(width: (width/2), height: (height/2))
    }
    
    init(viewModel: DishwasherListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    // MARK: - Set up
    
    private func setupViews() {
        view.addSubview(collectionView)
        collectionView.constrainToSuperview()
        title = DishwasherHomeConstants.homeTitle.rawValue
        collectionView.delegate = self
        collectionView.dataSource = self
        bindViewModel()
        viewModel.loadDishwashers()
    }
    
    private func collectionViewLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        return layout
    }
    
    private func bindViewModel() {
        viewModel.stateDidChange.bind { [weak self] _ in
            guard let self else {
                return
            }
            
            if let error = self.viewModel.errorMessageToDisplay {
                DispatchQueue.main.async {
                    self.presentAlertWith(message: AlertConstants.message(error).text)
                }
            }
            DispatchQueue.main.async {
                self.loadingView.isHidden = !self.viewModel.isLoading
                
                self.collectionView.reloadData()
                self.title =  DishwasherHomeConstants.homeTitle.rawValue +  " (\(self.viewModel.numberOfItems()))"
                
                
            }
        }
    }
}

// MARK: - Extensions
extension DishwasherListViewController: UICollectionViewDelegate,
                                        UICollectionViewDataSource,
                                        UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: view.identifierForCells, for: indexPath) as? DishwasherCollectionCell else { return UICollectionViewCell() }
        
        cell.configureWith(viewModel:viewModel.dataForItemAt(indexPath.row))
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectItemAt(indexPath.row)
    }
    
}


