//
//  DishwasherDetailCellViewModel.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

extension DishwasherDetailCell {
    struct ViewModel {
        let canExpand:Bool
        var isExpanded:Bool
        let title:String?
        let subTitle:String?
        let tapAction:(()->())?
    }
}
