//
//  DishwasherDetailCell.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

class DishwasherDetailCell: UITableViewCell {
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .caption1)
        return label
    }()
    
    private lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .caption2)
        return label
    }()
    
    private lazy var button:UIButton = UIButton(type: .system)
    
    private let horizontalStackView = UIStackView()
    private let verticalStackView = UIStackView()
    private var viewModel:ViewModel?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        subTitleLabel.text = nil
        button.removeFromSuperview()
    }
    
    private func setupViews() {
        contentView.addSubview(horizontalStackView)
        horizontalStackView.constrainToSuperview(topPadding: 8, bottomPadding: 8, leftPadding: 16, rightPadding: 16)
        horizontalStackView.axis = .horizontal
        horizontalStackView.distribution = .equalSpacing
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.axis = .vertical
        horizontalStackView.addArrangedSubview(verticalStackView)
        horizontalStackView.addArrangedSubview(subTitleLabel)
        titleLabel.numberOfLines = 4
    }
    
    
    func configureWith(viewModel:ViewModel) {
        titleLabel.text = viewModel.title
        subTitleLabel.text = viewModel.subTitle
        
        if viewModel.canExpand {
            button.setTitle(viewModel.isExpanded ?"Read less" :"Read more", for: .normal)
            button.addTarget(self, action: #selector(expandButtonTapped), for: .touchUpInside)
            self.viewModel = viewModel
            
            titleLabel.numberOfLines = viewModel.isExpanded ? 0 : 4

            verticalStackView.addArrangedSubview(button)
        }
    }
    
    @objc func expandButtonTapped() {
        viewModel?.tapAction?()
    }
}

