//
//  DishwasherDetailView.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

final class DishwasherDetailPriceView: UIView {
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .headline)
        label.font = .systemFont(ofSize: 17, weight: .bold)
        label.adjustsFontSizeToFitWidth = true
        
        return label
    }()
    
    private lazy var claimsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .red
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .subheadline)
        return label
    }()
    
    private lazy var guaranteeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.numberOfLines = 0
        return label
    }()
    
    private let stackView : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        return stack
    }()
    
    init() {
        super.init(frame: .zero)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(stackView)
        stackView.constrainToSuperview()
        stackView.addArrangedSubview(priceLabel)
        stackView.addArrangedSubview(claimsLabel)
        stackView.addArrangedSubview(guaranteeLabel)
    }
    
    func configureView(with viewModel: ProductInformationPresentationViewModel) {
        priceLabel.text = viewModel.price
        
        guard let claims = viewModel.claims,
              let guarantee = viewModel.guarantee else {
            guaranteeLabel.isHidden = true
            claimsLabel.isHidden = true
            return
        }
        guaranteeLabel.text = guarantee
        claimsLabel.text = claims
        
        setNeedsDisplay()
    }
}
