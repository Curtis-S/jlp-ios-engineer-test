//
//  DishwasherDetailViewController.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import UIKit

class DishwasherDetailViewController: UIViewController {
    private let viewModel: DishwasherDetailViewModel
    private let tableView = UITableView(frame: .zero, style: .grouped)
    private let productView = DishwasherDetailPriceView()
    
    private lazy var loadingView: LoadingView = {
        let loadView = LoadingView()
        view.addSubview(loadView)
        loadView.constrainToSuperview()
        return loadView
    }()
    
    private lazy var carouselView: CarouselView = {
        let carouselView = CarouselView(frame: CGRect(x: 0, y: topBarHeight, width: view.frame.width, height: view.frame.height/4))
        return carouselView
    }()
    
    private var topBarHeight: CGFloat {
        return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    // MARK: - Init
    init(viewModel: DishwasherDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        carouselView.frame = CGRect(x: 0, y: topBarHeight, width: view.frame.width, height: view.frame.height/4)
        carouselView.setNeedsDisplay()
        carouselView.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        carouselView.frame = CGRect(x: 0, y: topBarHeight, width: view.frame.width, height: view.frame.height/4)
        carouselView.setNeedsDisplay()
        carouselView.layoutIfNeeded()
    }
    
    // MARK: - Set up
    private func bindViewModel() {
        viewModel.stateDidChange.bind { [weak self] _ in
            guard let self else {
                return
            }
            
            if let error = self.viewModel.errorMessageToDisplay {
                DispatchQueue.main.async {
                    self.presentAlertWith(message: AlertConstants.message(error).text)
                }
            }
            DispatchQueue.main.async {
                self.loadingView.isHidden = !self.viewModel.isLoading
                    self.tableView.reloadData()
            }
        }
    }
    
    private func setupViews() {
        view.addSubview(carouselView)
        view.addSubview(tableView)
        view.addSubview(productView)
        
        setupProductView()
        setupTableView()
        title = viewModel.navigationBarTitle
        carouselView.setImagesForCarousel(imageUrls: viewModel.imagesForCarousel())
        productView.configureView(with: viewModel.productViewModel)
        bindViewModel()
        viewModel.loadDishwasherInformation()
    }
    
    private  func setupProductView() {
        productView.anchor(top: carouselView.bottomAnchor, paddingTop: 32,
                           left: view.leftAnchor,
                           paddingLeft: 16,
                           right: view.rightAnchor,
                           paddingRight: 16)
    }
    
    private func setupTableView() {
        tableView.anchor(top:productView.bottomAnchor, paddingTop: 8, bottom: view.bottomAnchor, left: view.leftAnchor, right: view.rightAnchor)
        tableView.register(DishwasherDetailCell.self, forCellReuseIdentifier: String(describing: DishwasherDetailCell.self))
        view.backgroundColor = .white
        tableView.dataSource = self
        tableView.delegate = self
    }
    
}

// MARK: - Delegate Extensions 
extension DishwasherDetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRowsForSection(section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        viewModel.titleForSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DishwasherDetailCell.self)) as? DishwasherDetailCell {
            cell.selectionStyle = .none
            let viewModel = viewModel.cellViewModels[indexPath.section][indexPath.row]
            cell.configureWith(viewModel: viewModel)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView.numberOfSections > 1 ,
              indexPath.section == 0  else {
            return
        }
        viewModel.readMoreButtonTapped()
        
    }
    
}


