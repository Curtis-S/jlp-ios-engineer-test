//
//  DishwasherDetailViewModel .swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

final class DishwasherDetailViewModel {
    private var dishwasher: Dishwasher
    private var  washerDetailLoader: DishwasherDetailRetrievalService
    private var cellRowTitles = DishwasherDetailCellConstants.allCases
    var cellViewModels: [[DishwasherDetailCell.ViewModel]] {
        makeViewModels()
    }
    var isLoading = false
    var stateDidChange = Observable(())
    
    var errorMessageToDisplay: Error? = nil
    var navigationBarTitle: String?  { dishwasher.name }
    var productViewModel: ProductInformationPresentationViewModel {
        ProductInformationPresentationViewModel(dishwasher: dishwasher)
    }
    var expandProductDescription:Bool = false
    
    private var productHasDescription: Bool {
        dishwasher.productInformation != nil
    }
    
    init(dishwasher: Dishwasher, loader: DishwasherDetailRetrievalService) {
        self.dishwasher = dishwasher
        self.washerDetailLoader = loader
    }
    
    func loadDishwasherInformation()  {
        isLoading = true
        stateDidChange.value = ()
        washerDetailLoader.fetchDishwasherDetails(using: dishwasher.productId){ [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let details):
                self.dishwasher.productInformation = details?.productInformation.html2String
                self.isLoading = false
                stateDidChange.value = ()
            case .failure(let error):
                self.isLoading = false
                self.errorMessageToDisplay = error
                stateDidChange.value = ()
            }
        }
    }
    
    
    func numberOfRowsForSection(_ section:Int ) -> Int {
        cellViewModels[section].count
    }
    
    func titleForSection(_ section: Int) -> String {
        guard productHasDescription else {
            return "Product specification"
        }
        
        if section == 0 {
            return "Product information"
        } else {
            return "Product specification"
        }
    }
    
    func numberOfSections() -> Int {
        productHasDescription ? 2 : 1
    }
    
    
    func readMoreButtonTapped() {
        guard productHasDescription else { return }
        expandProductDescription.toggle()
        stateDidChange.value = ()
    }
    
    func imagesForCarousel() ->[URL] {
        var imagesUrlsToReturn = [URL]()
        
        guard let imageUrls  = dishwasher.alternateImageUrls,
              let mainImageUrl = dishwasher.mainImageURL else {
            return imagesUrlsToReturn
        }
        if let mainUrl = URL(string: mainImageUrl.addHttpToString()) {
            imagesUrlsToReturn.append(mainUrl)
        }
        
        
        for imageUrl in imageUrls {
            if let url = URL(string: imageUrl.addHttpToString()) {
                imagesUrlsToReturn.append(url)
            }
        }
        return imagesUrlsToReturn
    }
    
    //MARK: - helper
    
    private func makeViewModels() -> [[DishwasherDetailCell.ViewModel]]   {
        var cellViewModels: [[DishwasherDetailCell.ViewModel]] = []
        if productHasDescription {
            let firstSection = [DishwasherDetailCell.ViewModel(canExpand: true, isExpanded: expandProductDescription, title: dishwasher.productInformation, subTitle: nil, tapAction: { [weak self] in
                self?.readMoreButtonTapped()
            })]
            cellViewModels.append(firstSection)
        }
        var sectionSection = [DishwasherDetailCell.ViewModel]()
        for title in cellRowTitles {
            sectionSection.append(.init(canExpand: false, isExpanded: false, title: title.text, subTitle: title.textForRowSubtitleUsing(dishwasher: dishwasher), tapAction: nil))
            
        }
        cellViewModels.append(sectionSection)
        
        return cellViewModels
    }
    
}
