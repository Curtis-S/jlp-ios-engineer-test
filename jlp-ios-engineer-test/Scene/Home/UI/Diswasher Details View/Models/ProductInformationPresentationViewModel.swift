//
//  ProductInformationPresentationViewModel.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

struct ProductInformationPresentationViewModel {
    let price: String?
    let claims: String?
    let guarantee: String?
    
    init(dishwasher: Dishwasher) {
        price = dishwasher.price
        claims = dishwasher.claimsText
        guarantee = dishwasher.guaranteeText
    }
}
