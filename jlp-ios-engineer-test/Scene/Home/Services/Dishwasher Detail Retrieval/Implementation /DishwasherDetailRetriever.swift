//
//  DishwasherDetailRetriever.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

final class DishwasherDetailRetriever: DishwasherDetailRetrievalService {
    private let dishwasherDetailsMapper:DishwasherDetailsMapper
    private let httpClient: HttpClient
    
     init(dishwasherDetailsMapper: DishwasherDetailsMapper, httpClient: HttpClient) {
        self.dishwasherDetailsMapper = dishwasherDetailsMapper
        self.httpClient = httpClient
    }
   
    enum Error: Swift.Error {
        case connectivity
        case invalidData
    }
    
    func fetchDishwasherDetails(using diswasherId:String, completion: @escaping (DishwasherDetailsCompletion)-> Void) {
        Task {
            let url = DishwasherApiEndpoint.detail.url
            let request = URLRequest(url: url)
            guard let result = try? await httpClient.data(for: request) else  {
                completion(.failure(Error.connectivity))
                return
            }
            
            guard let diswasherDetailList:[DishwasherDetails] = try? dishwasherDetailsMapper.map(data: result.data, urlResponse: result.response) else {
                completion(.failure(Error.invalidData))
                return
            }
            
            let matchedDishwasher = diswasherDetailList.first {
                $0.id == diswasherId
            }
            
            completion(.success(matchedDishwasher))
        }
        
    }
}
