//
//  DishwasherDetailRetrievalService.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
typealias DishwasherDetailsCompletion = Result<DishwasherDetails?,Error>

///  Responsible for fetching the details of a dishwasher
protocol DishwasherDetailRetrievalService {
    ///  Fetches the detail object for the provided dishwasher id (if any)
    func fetchDishwasherDetails(using dishwasherId :String, completion: @escaping (DishwasherDetailsCompletion) -> Void)
}
