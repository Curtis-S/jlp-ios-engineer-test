//
//  DefaultDishwasherDetailsMapper.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation


/// A mapper component to be used for parsing data to Dishwasher detail objects
protocol DishwasherDetailsMapper:NetworkResponseMapper  {
    func map(data: Data, urlResponse: HTTPURLResponse) throws -> [DishwasherDetails]
}

class DefaultDishwasherDetailsMapper:DishwasherDetailsMapper{
    private struct Root: Decodable {
        private let detailsData: [ProductDetails]
        private struct ProductDetails: Decodable {
            let productId: String
            let details:Details
            
            struct Details :Decodable {
                let productInformation:String?
            }
        }
        
        var dishwasherDetails: [DishwasherDetails]  { detailsData.map {
            DishwasherDetails(id: $0.productId, productInformation: $0.details.productInformation ?? "")
        }}
    }
    
    enum Error: Swift.Error {
        case invalidData
    }
    
    func map(data: Data, urlResponse: HTTPURLResponse) throws -> [DishwasherDetails] {
        let decoder = JSONDecoder()
        let response: Root = try parse(data, urlResponse: urlResponse, with: decoder)
        
        return response.dishwasherDetails
    }
}
