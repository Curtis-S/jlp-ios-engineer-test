//
//  DishwasherListMapper.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

/// A mapper component to be used for parsing data to Dishwasher objects
protocol DishwasherListMapper: NetworkResponseMapper {
    /// maps the data to  dishwasher objects
    /// - Parameters:
    ///   - data: json data
    ///   - urlResponse: url response
    /// - Returns: List of dishwasher objects
    func map(data: Data, urlResponse: HTTPURLResponse) throws -> [Dishwasher]
}

struct DefaultDishwasherListMapper:DishwasherListMapper {
    func map(data: Data, urlResponse: HTTPURLResponse) throws -> [Dishwasher] {
        let decoder = JSONDecoder()
        let response: Root = try parse(data, urlResponse: urlResponse, with: decoder)
        
        return response.dishwashers
    }
    
 //MARK: - Decodable model
    private struct Root: Decodable {
        private let products: [RemoteDishwasherProduct]
        
        private struct RemoteDishwasherProduct: Decodable {
            let productId: String
            let title: String?
            let image: String?
            let alternativeImageUrls: [String]?
            let price: Price
            let displaySpecialOffer: String?
            let  dynamicAttributes: DynamicAttributes?
        }
        
        private struct Price: Decodable {
            let now: String
        }
        private struct Value: Decodable {
            let max: String?
        }
        
      
        
        private struct DynamicAttributes: Decodable {
            let timerdelay: String?
            let childlock: String?
            let dimensions: String?
            let dryingperformance: String?
            let adjustableracking: String?
            let guarantee: String?
            let dryingsystem: String?
            let delicatewash: String?
        }
        
        var dishwashers: [Dishwasher] {
            products.map {
                Dishwasher(productId:  $0.productId, name: $0.title ?? "",
                           price: $0.price.now,
                           claimsText: $0.displaySpecialOffer, guaranteeText: $0.dynamicAttributes?.guarantee,
                           productInformation: nil, adjustableRackingInfo: $0.dynamicAttributes?.adjustableracking ,
                           childLock: $0.dynamicAttributes?.childlock , delayWash: $0.dynamicAttributes?.timerdelay ,
                           dimensions: $0.dynamicAttributes?.dimensions, dryingPerformance:  $0.dynamicAttributes?.dryingperformance,
                           dryingSystem:  $0.dynamicAttributes?.dryingsystem, mainImageURL: $0.image,
                           alternateImageUrls: $0.alternativeImageUrls,
                           delicateWash: $0.dynamicAttributes?.delicatewash)
            }
        }
    }
}



