//
//  DishwasherListRetriever.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

class DishwasherListRetriever:DishwasherListRetrievalService {
    private let dishWasherListMapper:DishwasherListMapper
    private let httpClient:HttpClient
   
    init(dishWasherListMapper: DishwasherListMapper,
         httpClient: HttpClient
    ) {
        self.dishWasherListMapper = dishWasherListMapper
        self.httpClient = httpClient
    }
    

    func fetchDishwashers(completion: @escaping (DishwasherListRetrieverCompletion)-> Void) {
        
        Task {
            let url = DishwasherApiEndpoint.list.url
            let request = URLRequest(url: url)
            guard let result = try? await httpClient.data(for: request) else  {
                completion(.failure(Error.connectivity))
                return
            }
            
            guard let dishwasherList:[Dishwasher] = try? dishWasherListMapper.map(data: result.data, urlResponse: result.response) else {
                completion(.failure(Error.invalidData))
                return
            }
            
            completion(.success(dishwasherList))
        }
        
    }

    
    enum Error: Swift.Error {
        case connectivity
        case invalidData
    }
}
