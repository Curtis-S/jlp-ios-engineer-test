//
//  DishwasherListRetrievalService.swift
//  jlp-ios-engineer-test
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation

typealias DishwasherListRetrieverCompletion = Result<[Dishwasher],Error>

///  Responsible for fetching the list of dishwasher objects
protocol DishwasherListRetrievalService {
    func fetchDishwashers(completion: @escaping (DishwasherListRetrieverCompletion)-> Void)
}
