# JLP iOS Engineer Test Submission (Curtis Scott)

## Welcome

Thank you very much for taking the time to review my test, I look forward to hearing from you :).

## Project

On initial app launch the customer is presented with a grid of dishwashers available for purchase at John Lewis.
Then when the dishwasher taps on the product grid a new screen is displayed that displays the details about that particular product.

## Assumptions / Notes

* Focus was put on trying making the code modular.
* I only made some properties non optional as not sure if it would be there for all products 
* Added accessibility labels to the images to add a bit more context.
* Views were done in UIKit with programatic views (didn't think I was allowed to use swift UI else would have been glad to). 
* Most of it was done in a TDD style approach. The views or viewControllers are not tested but effort was taken to try and keep logic out of them.
* The data displayed was trying to replicate the provided images. 
* Rotation is only allowed on the iPad.
* The only edge case errors accounted for are connectivity and invalid data
* SdWebImage was used for images just to show familiarity of packages even though it may have been overkill for this but the image caching out the box is nice :)
* The details endpoint didn't have details for all products so I had to only return what ever matches the id 
* The readme basically said not to spend too long on this so I tried to limit my self so I had to compromise on testing less important components and getting the improving the UI for times sake. But still focusing on delivering top quality code. 
* UI tests was removed because none was written 

## Possible Improvements 

* Add more unit tests (to the view controllers also) to increase coverage and add more edge cases 
* Add snap tests to add more confidence to UI
* Add integration tests
* More helper functions and strings for tests to reduce repetition 
* UI improvements following designs from UI/UX team
* Use Swift UI for the views to reduce code 
* Add a Linter to minimise spelling, spacing or other small errors 
* If swift UI cannot be used consider changing to differable datasources for easier animations 
* improve on accessibility to be more informative and helpful 
* Better file organisation 
* Explore DI libraries 
* Centralising constant strings (for test also) to reduce repetition
* Consider localisation 
* Test Helper Protocols for similar components eg mapper or coordinator 



