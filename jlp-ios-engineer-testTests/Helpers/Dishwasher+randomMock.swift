//
//  Dishwasher+randomMock.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

extension Dishwasher {
    static func randomMock() -> Dishwasher {
        .init(productId: UUID().uuidString,
              name: "TestName",
              price: "300",
              claimsText:"TestClaimsText",
              guaranteeText: "TestGuaranteeText",
              productInformation: nil,
              adjustableRackingInfo: "ajustableRackingInfoYes",
              childLock: " childLockText",
              delayWash:"delayWashText",
              dimensions:"dimensionsText",
              dryingPerformance:"dryingPerformanceText",
              dryingSystem: "dryingSystemText",
              mainImageURL: "mainImageURLText",
              alternateImageUrls: ["testurl.com","testurl2.com"],
              
              delicateWash: "RandomText"
)
    }
}

