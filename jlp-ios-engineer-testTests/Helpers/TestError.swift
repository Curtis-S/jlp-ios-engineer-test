//
//  TestError.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 12/02/2024.
//

import Foundation

enum TestError:Error {
    case mock
}
