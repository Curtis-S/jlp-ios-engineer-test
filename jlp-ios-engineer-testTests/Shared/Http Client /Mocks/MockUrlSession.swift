//
//  MockUrlSession.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

class MockUrlSession:ClientSession {
    private(set) var dataForRequest:URLRequest?
    var errorToThrow:Error?
    var dataToReturn:(Data, HTTPURLResponse)?
    func data(for request: URLRequest) async throws -> (Data, URLResponse) {
        dataForRequest = request
        if let errorToThrow {
            throw errorToThrow
        }
        if let dataToReturn {
           return  dataToReturn
        }

        return (Data(),HTTPURLResponse())
    }
}
