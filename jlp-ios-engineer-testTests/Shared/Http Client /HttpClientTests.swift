//
//  HttpClientTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class HttpClientTests: XCTestCase {
    func makeSUT(session:ClientSession = MockUrlSession()) -> DefaultHttpClient {
        DefaultHttpClient(session:session)
    }
    
    func testPassesCorrectRequestToSession() async throws {
        let mockSession = MockUrlSession()
        let sut = makeSUT(session: mockSession)
        let mockUrl = try XCTUnwrap(URL(string: "www.anyurl.com"))
        let request = URLRequest(url: mockUrl)
        
       _ =  try await  sut.data(for: request)
        
        XCTAssertEqual(request, mockSession.dataForRequest)
    }
    
    func testDataForReturnsCorrectInformationFromTests() async throws {
        let mockSession = MockUrlSession()
        let mockData = try XCTUnwrap("Test".data(using: .utf8))
        let mockDataToReturn = (data: mockData,response: HTTPURLResponse())
        mockSession.dataToReturn = mockDataToReturn
        let sut = makeSUT(session: mockSession)
        let mockUrl = try XCTUnwrap(URL(string: "www.anyurl.com"))
        let request = URLRequest(url: mockUrl)
        
       let respose =  try await sut.data(for: request)
        
        XCTAssertEqual(respose.data, mockDataToReturn.data)
        XCTAssertEqual(respose.response, mockDataToReturn.response)
    }
    
    func testDataForReturnsCorrectErrorFromSession() async throws {
        let mockSession = MockUrlSession()
        mockSession.errorToThrow = TestError.mock
        let sut = makeSUT(session: mockSession)
        let mockUrl = try XCTUnwrap(URL(string: "www.anyurl.com"))
        let request = URLRequest(url: mockUrl)
        
        do {
           _ = try await sut.data(for: request)
            XCTFail("sut should have thrown an error")
        } catch TestError.mock {
            
        } catch {
            XCTFail(" should have thrown \(TestError.mock) but it threw \(error)")
        }
        
    }
}

