//
//  NetworkResponseMapperTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class NetworkResponseMapperTests: XCTestCase {
    func makeSUT() -> any NetworkResponseMapper {
       AnyMapper()
    }
    
    func testCheckForTwoHunded() throws {
        let sut = makeSUT()
        let anyUrl = try XCTUnwrap(URL(string: "www.anyurl.som"))
        let mockValidStatusCodeValues:[Int] = [200,201,203,299]
        let mockInvaidStatusCodeValues:[Int] = [100,300,199,400,500]
        
        for statusCode in mockValidStatusCodeValues {
            let response = try XCTUnwrap(HTTPURLResponse(url: anyUrl, statusCode: statusCode, httpVersion:  nil, headerFields: nil))
            XCTAssertTrue(sut.checkForTwoHundredHttpResponse(using:response))
        }
        
        for statusCode in mockInvaidStatusCodeValues {
            let response = try XCTUnwrap(HTTPURLResponse(url: anyUrl, statusCode: statusCode, httpVersion:  nil, headerFields: nil))
            XCTAssertFalse(sut.checkForTwoHundredHttpResponse(using:response))
        }
    }
    
    func testParseFunction() throws {
        let encoder = JSONEncoder()
        let decoder = JSONDecoder()
        let sut = makeSUT()
        let mockString = "Any String"
        let dataString = try encoder.encode(mockString)
        
        let result:String = try sut.parse(dataString, urlResponse: .init(), with: decoder)
        
        XCTAssertEqual(mockString, result)
        
        
         
    }
    
    func testParseFunctionThrowsCorrectErrors() throws {
        let encoder = JSONEncoder()
        let decoder = JSONDecoder()
        let sut = makeSUT()
        let mockString = "Any String"
        let dataString = try encoder.encode(mockString)
        let anyUrl = try XCTUnwrap(URL(string: "www.anyurl.som"))
        let invalidResponse = try XCTUnwrap(HTTPURLResponse(url: anyUrl, statusCode: 404, httpVersion:  nil, headerFields: nil))
        
        do {
            let _:Int = try sut.parse(dataString, urlResponse: .init(), with: decoder)
            XCTFail("expected \( NetworkResponseMapperError.parsingError )")
        } catch NetworkResponseMapperError.parsingError {
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.parsingError ) but got \(error)")
        }
      
        do {
            let _:String = try sut.parse(dataString, urlResponse:invalidResponse, with: decoder)
            XCTFail("expected \( NetworkResponseMapperError.parsingError )")
        } catch NetworkResponseMapperError.nonTwoHundredErrorCode {
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode ) but got \(error)")
        }
    }
    
    //MARK: - any mapper
    // to test the functions
    private struct AnyMapper:NetworkResponseMapper {
        
        func map(data: Data, urlResponse: HTTPURLResponse) throws -> String {
            ""
        }
        typealias MappedModel = String
    }
}
