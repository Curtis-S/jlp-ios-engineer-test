//
//  DishwasherListViewModelTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class DishWasherListViewModelTests: XCTestCase {

    func makeSUT(dishwasherRetriever: DishwasherListRetrievalService = DishwasherListRetrieverSpy(),
                 delegate: DishwasherListViewModelDelegate =  DishWasherListViewModelDelegateSpy()) -> DishwasherListViewModel {
        DishwasherListViewModel(washerLoader: dishwasherRetriever, delegate: delegate)
    }
    
    func testLoadDishwashersReceivesListOfWashersFromRetrieverService() {
        let mockDishwashers:[Dishwasher] = [.randomMock(),.randomMock()]
        let mockRetriever = DishwasherListRetrieverSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever)
        
        sut.loadDishwashers()
        mockRetriever.completeFetching(with: .success(mockDishwashers))
        
        XCTAssertEqual( sut.numberOfItems() , 2)
    }
    
    func testLoadingStates() {
        let mockDishwashers:[Dishwasher] = [.randomMock(),.randomMock()]
        let mockRetriever = DishwasherListRetrieverSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever)
        XCTAssertFalse(sut.isLoading)
        
        sut.loadDishwashers()
        
        XCTAssertTrue(sut.isLoading)
        
        mockRetriever.completeFetching(with: .success(mockDishwashers))
        
        XCTAssertFalse(sut.isLoading)
        
    
    }
    
    func testCellViewModelCreation() {
        let mockDishwashers:[Dishwasher] = [.randomMock(),.randomMock()]
        let mockRetriever = DishwasherListRetrieverSpy()
        let mockDelegate = DishWasherListViewModelDelegateSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever,delegate: mockDelegate)
        XCTAssertFalse(sut.isLoading)
        sut.loadDishwashers()
        mockRetriever.completeFetching(with: .success(mockDishwashers))
        
        let result = sut.dataForItemAt(0)
        
        XCTAssertEqual(sut.numberOfItems(), mockDishwashers.count)
        XCTAssertEqual(sut.numberOfSections(), 1)
        XCTAssertEqual(result.name, mockDishwashers.first?.name)
        XCTAssertEqual(result.price, mockDishwashers.first?.price)
        XCTAssertEqual(result.imageUrl, mockDishwashers.first?.mainImageURL)
    }
    
    func testSelectedItemCallsDelegate() {
        let mockDishwashers:[Dishwasher] = [.randomMock(),.randomMock()]
        let mockRetriever = DishwasherListRetrieverSpy()
        let mockDelegate = DishWasherListViewModelDelegateSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever,delegate: mockDelegate)
        XCTAssertFalse(sut.isLoading)
        sut.loadDishwashers()
        mockRetriever.completeFetching(with: .success(mockDishwashers))
        
        sut.didSelectItemAt(0)
        
        XCTAssert(mockDelegate.showDetailForItemCalled)
        XCTAssertEqual(mockDelegate.showDetailForItemReceivedItem, mockDishwashers.first)
        XCTAssertEqual(mockDelegate.showDetailForItemCallsCount, 1)
    }
    
    func testDidSelectItemAtDoesNotCallDelegateIfRowDoesNotExist() {
        let mockDishwashers:[Dishwasher] = [.randomMock(),.randomMock()]
        let mockRetriever = DishwasherListRetrieverSpy()
        let mockDelegate = DishWasherListViewModelDelegateSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever,delegate: mockDelegate)
        XCTAssertFalse(sut.isLoading)
        sut.loadDishwashers()
        mockRetriever.completeFetching(with: .success(mockDishwashers))
        
        sut.didSelectItemAt(99) // invalid index
        sut.didSelectItemAt(5) // invalid index
        sut.didSelectItemAt(2) // invalid index
        sut.didSelectItemAt(6) // invalid index
        sut.didSelectItemAt(9) // invalid index
        
        XCTAssertFalse(mockDelegate.showDetailForItemCalled)
        XCTAssertEqual(mockDelegate.showDetailForItemReceivedItem, nil)
    
    }
    
    
    func testDataLoadingError() {
        let mockRetriever = DishwasherListRetrieverSpy()
        let sut = makeSUT(dishwasherRetriever: mockRetriever)
        XCTAssertNil(sut.errorMessageToDisplay)
        
        sut.loadDishwashers()
        mockRetriever.completeFetching(with: .failure(TestError.mock))
        
        XCTAssertNotNil(sut.errorMessageToDisplay)
    }

}









