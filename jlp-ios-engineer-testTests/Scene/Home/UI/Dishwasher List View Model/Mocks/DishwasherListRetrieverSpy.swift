//
//  DishwasherListRetrieverSpy.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

class DishwasherListRetrieverSpy: DishwasherListRetrievalService {
    var fetchDishwashersCompletionCallsCount = 0
    var fetchDishwashersCompletionCalled: Bool {
        return fetchDishwashersCompletionCallsCount > 0
    }
   private var fetchDishwashersCompletionClosure: ((DishwasherListRetrieverCompletion) -> Void)?
  
    func fetchDishwashers(completion: @escaping (DishwasherListRetrieverCompletion) -> Void) {
        fetchDishwashersCompletionCallsCount += 1
        fetchDishwashersCompletionClosure = completion
    }
    
    func completeFetching(with completion:DishwasherListRetrieverCompletion) {
        fetchDishwashersCompletionClosure?(completion)
    }
}
