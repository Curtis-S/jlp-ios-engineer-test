//
//  DishWasherListViewModelDelegateSpy.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

class DishWasherListViewModelDelegateSpy: DishwasherListViewModelDelegate {
    var showDetailForItemCallsCount = 0
    var showDetailForItemCalled: Bool {
        return showDetailForItemCallsCount > 0
    }
    var showDetailForItemReceivedItem: Dishwasher?
    var showDetailForItemClosure: ((Dishwasher) -> Void)?
    
    func showDetailFor(item: Dishwasher) {
        showDetailForItemCallsCount += 1
        showDetailForItemReceivedItem = (item)
        showDetailForItemClosure?(item)
    }
}
