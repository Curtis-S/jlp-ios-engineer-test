//
//  DishwasherDetailViewModelTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import XCTest
@testable import jlp_ios_engineer_test

class DishwasherDetailViewModelTests: XCTestCase {
    func makeSUT(dishwasher:Dishwasher = .randomMock(),loader: DishwasherDetailRetrievalService = DishwasherDetailsRetrieverServiceSpy()) -> DishwasherDetailViewModel {
        DishwasherDetailViewModel(dishwasher: dishwasher, loader:loader)
    }
    
    
    func testLoadingStates() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let sut = makeSUT(loader: mockRetriever)
        XCTAssertFalse(sut.isLoading)
        
        sut.loadDishwasherInformation()
        
        XCTAssertTrue(sut.isLoading)
        
        mockRetriever.completeFetching(with: .success(nil))
        
        XCTAssertFalse(sut.isLoading)
        
    }
    
    func testReadMoreTappedTogglesExpandProductDescription() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let mockDishwasherDetails = DishwasherDetails(id: mockDishwasher.productId, productInformation: "testInfo")
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
        
        sut.loadDishwasherInformation()
        mockRetriever.completeFetching(with: .success(mockDishwasherDetails))
        
        XCTAssertFalse(sut.expandProductDescription)
        
        sut.readMoreButtonTapped()
        
        XCTAssert(sut.expandProductDescription)
    }
    
    func testTitleForSection() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let mockDishwasherDetails = DishwasherDetails(id: mockDishwasher.productId, productInformation: "testInfo")
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
        
        // should be "Product specification" when there is no description
        XCTAssertEqual(sut.titleForSection(0), "Product specification")
        
        sut.loadDishwasherInformation()
        mockRetriever.completeFetching(with: .success(mockDishwasherDetails))
        
        // should be"Product information" when there is a  description
        XCTAssertEqual(sut.titleForSection(0), "Product information")
    }
    
    func testCellModelsDisplayCorrectInformationWhenProductInfoIsAvailable() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let mockDishwasherDetails = DishwasherDetails(id: mockDishwasher.productId, productInformation: "testInfo")
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
        XCTAssertFalse(sut.isLoading)
        
        XCTAssertEqual(sut.numberOfSections(), 1)
        XCTAssertEqual(sut.numberOfRowsForSection(0), 7)
        
    // order "Adjustable racking"  "Child lock" "Delay wash"  "Delicate wash"
    //  "Dimensions"  "Drying performance" "Drying system"

        XCTAssertEqual(mockDishwasher.adjustableRackingInfo, sut.cellViewModels[0][0].subTitle)
        XCTAssertEqual(mockDishwasher.childLock, sut.cellViewModels[0][1].subTitle)
        XCTAssertEqual(mockDishwasher.delayWash, sut.cellViewModels[0][2].subTitle)
        XCTAssertEqual(mockDishwasher.delicateWash, sut.cellViewModels[0][3].subTitle)
        XCTAssertEqual(mockDishwasher.dimensions, sut.cellViewModels[0][4].subTitle)
        XCTAssertEqual(mockDishwasher.dryingPerformance, sut.cellViewModels[0][5].subTitle)
        XCTAssertEqual(mockDishwasher.dryingSystem, sut.cellViewModels[0][6].subTitle)
    
                sut.loadDishwasherInformation()
                mockRetriever.completeFetching(with: .success(mockDishwasherDetails))
  
        // when data information is received add it to first section
        XCTAssertEqual(sut.numberOfSections(), 2)
        XCTAssertEqual(sut.numberOfRowsForSection(0), 1)
        XCTAssertEqual(sut.numberOfRowsForSection(1), 7)
        
        XCTAssertEqual(mockDishwasherDetails.productInformation, sut.cellViewModels[0][0].title)
    
    }
    
    func testCellModelsDisplayCorrectInformationWhenProductInfoIsNotAvailable() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
        XCTAssertFalse(sut.isLoading)
        
        XCTAssertEqual(sut.numberOfSections(), 1)
        XCTAssertEqual(sut.numberOfRowsForSection(0), 7)
        
    // order "Adjustable racking"  "Child lock" "Delay wash"  "Delicate wash"
    //  "Dimensions"  "Drying performance" "Drying system"

        XCTAssertEqual(mockDishwasher.adjustableRackingInfo, sut.cellViewModels[0][0].subTitle)
        XCTAssertEqual(mockDishwasher.childLock, sut.cellViewModels[0][1].subTitle)
        XCTAssertEqual(mockDishwasher.delayWash, sut.cellViewModels[0][2].subTitle)
        XCTAssertEqual(mockDishwasher.delicateWash, sut.cellViewModels[0][3].subTitle)
        XCTAssertEqual(mockDishwasher.dimensions, sut.cellViewModels[0][4].subTitle)
        XCTAssertEqual(mockDishwasher.dryingPerformance, sut.cellViewModels[0][5].subTitle)
        XCTAssertEqual(mockDishwasher.dryingSystem, sut.cellViewModels[0][6].subTitle)
    
                sut.loadDishwasherInformation()
                mockRetriever.completeFetching(with: .success(nil))
  
        // when data information is  not received no sections added
        XCTAssertEqual(sut.numberOfSections(), 1)
        XCTAssertEqual(sut.numberOfRowsForSection(0), 7)
    }
    
    func testCaruselImageUrlCreation() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
       
        let result = sut.imagesForCarousel()
        
        XCTAssertEqual(result.map(\.absoluteString),["https:mainImageURLText", "https:testurl.com", "https:testurl2.com"])
    }
    
    func testDataParseError() {
        let mockRetriever = DishwasherDetailsRetrieverServiceSpy()
        let mockDishwasher = Dishwasher.randomMock()
        let sut = makeSUT(dishwasher: mockDishwasher,loader: mockRetriever)
        XCTAssertFalse(sut.isLoading)
        XCTAssertNil(sut.errorMessageToDisplay)
    
        sut.loadDishwasherInformation()
        XCTAssert(sut.isLoading)
        
        mockRetriever.completeFetching(with: .failure(TestError.mock))
        
        XCTAssertNotNil(sut.errorMessageToDisplay)
    }
    
}

