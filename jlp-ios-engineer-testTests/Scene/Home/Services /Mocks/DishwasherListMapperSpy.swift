//
//  DishwasherListMapperSpy.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

class DishwasherListMapperSpy: DishwasherListMapper {
    var mapDataUrlResponseCallsCount = 0
    var mapDataUrlResponseCalled: Bool {
        return mapDataUrlResponseCallsCount > 0
    }
    var mapDataUrlResponseReceivedArguments: (data: Data, urlResponse: HTTPURLResponse)?
    var mapDataUrlResponseThrowableError: Error?
    var mapDataUrlResponseReturnValue: [Dishwasher]?

    func map(data: Data, urlResponse: HTTPURLResponse) throws -> [Dishwasher] {
        mapDataUrlResponseCallsCount += 1
        mapDataUrlResponseReceivedArguments = (data, urlResponse)
        if let mapDataUrlResponseThrowableError {
            throw mapDataUrlResponseThrowableError
        }
        
        return mapDataUrlResponseReturnValue ?? []
        
    }
}
