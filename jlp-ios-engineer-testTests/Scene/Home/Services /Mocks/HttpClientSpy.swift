//
//  HttpClientSpy.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
@testable import jlp_ios_engineer_test

class HttpClientSpy: HttpClient {    
    var dataForCallsCount = 0
    var dataForCalled: Bool {
        return dataForCallsCount > 0
    }
    var dataForReceivedRequest: URLRequest?
    var dataForReceivedInvocations: [URLRequest] = []
    var dataForThrowableError: Error?
    var dataForReturnValue: NetworkingResponse?
    var dataForClosure: ((URLRequest) async throws -> NetworkingResponse)?
    func data(for request: URLRequest) async throws -> NetworkingResponse {
        dataForCallsCount += 1
        dataForReceivedRequest = (request)
        if let dataForThrowableError {
            throw dataForThrowableError
        }
            return dataForReturnValue ?? (Data(),HTTPURLResponse())
    }
}
