//
//   DishwasherDetailsRetrieverServiceSpy.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

@testable import jlp_ios_engineer_test

class DishwasherDetailsRetrieverServiceSpy: DishwasherDetailRetrievalService {
    var fetchDishwasherDetailsUsingCompletionCallsCount = 0
    var fetchDishwasherDetailsUsingCompletionCalled: Bool {
        return fetchDishwasherDetailsUsingCompletionCallsCount > 0
    }
    var fetchDishwasherDetailsUsingCompletionReceivedArguments: (dishwasherId : String, completion: (DishwasherDetailsCompletion) -> Void)?
 
    private var fetchDishwashersCompletionClosure: ((DishwasherDetailsCompletion) -> Void)?
     
     func fetchDishwasherDetails(using dishwasherId: String, completion: @escaping (DishwasherDetailsCompletion) -> Void) {
         fetchDishwasherDetailsUsingCompletionCallsCount += 1
         fetchDishwasherDetailsUsingCompletionReceivedArguments = (dishwasherId , completion)
         fetchDishwashersCompletionClosure = completion
     }
    
    func completeFetching(with completion:DishwasherDetailsCompletion) {
        fetchDishwashersCompletionClosure?(completion)
    }
  
}
