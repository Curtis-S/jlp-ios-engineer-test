//
//  DishwasherDetailLoaderTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class DishwasherDetailLoaderTests: XCTestCase {
    func makeSUT(httpClient: HttpClient = HttpClientSpy(),
                 dishWasherDetailsMapper: DishwasherDetailsMapper =  DishwasherDetailsMapperSpy()
    
    ) -> DishwasherDetailRetriever {
        DishwasherDetailRetriever(dishwasherDetailsMapper: dishWasherDetailsMapper, httpClient: httpClient)
    }
    
    
    func testFetchDishwasherDetailsCreatesCorrectRequest() {
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherDetailsMapperSpy()
        
        let sut = makeSUT(httpClient: mockHttpClient,dishWasherDetailsMapper: mockMapper)
        
        expect(sut, toCompleteWith: .success(nil), using: "testId")
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockMapper.mapDataUrlResponseCallsCount,1)
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockHttpClient.dataForReceivedRequest?.url?.absoluteString, DishwasherApiEndpoint.detail.url.absoluteString)
    }
    
    func testFetchDiswasherDetailIsWhatIsReturnedFromTheMapperIfThereIsAMatch()  {
        let mockDishwasherDetails = DishwasherDetails(id: "testId", productInformation: "test info")
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherDetailsMapperSpy()
        mockMapper.mapDataUrlResponseReturnValue = [mockDishwasherDetails]
        
        let sut = makeSUT(httpClient: mockHttpClient,dishWasherDetailsMapper: mockMapper)
        
        expect(sut, toCompleteWith: .success(mockDishwasherDetails), using: "testId")
        
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockMapper.mapDataUrlResponseCallsCount,1)
        XCTAssert(mockMapper.mapDataUrlResponseCalled)

    }
    
    func testFetchDishwasherDetailIsNilWhatIfThereIsNoMatch()  {
        let mockDishwasherDetails = DishwasherDetails(id: "testId", productInformation: "test info")
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherDetailsMapperSpy()
        mockMapper.mapDataUrlResponseReturnValue = [mockDishwasherDetails]
        
        let sut = makeSUT(httpClient: mockHttpClient,dishWasherDetailsMapper: mockMapper)
        
        expect(sut, toCompleteWith: .success(nil), using: "randomId")
        
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockMapper.mapDataUrlResponseCallsCount,1)
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
    }
    
    //MARK: - error tests

    func testFetchDetialsDeliversConnectivityError()  {
        let mockDishwasherDetails = DishwasherDetails(id: "testId", productInformation: "test info")
        let mockHttpClient = HttpClientSpy()
        mockHttpClient.dataForThrowableError = TestError.mock
        let mockMapper = DishwasherDetailsMapperSpy()
        mockMapper.mapDataUrlResponseReturnValue = [mockDishwasherDetails]
        
        let sut = makeSUT(httpClient: mockHttpClient,dishWasherDetailsMapper: mockMapper)
        
        expect(sut, toCompleteWith: .failure(DishwasherDetailRetriever.Error.connectivity), using: "testId")
        
    }
    
    func testFetchDetailsDeliversConnectivityDataErrorWhenMappingFails()  {
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherDetailsMapperSpy()
        mockMapper.mapDataUrlResponseThrowableError = TestError.mock
        
        let sut = makeSUT(httpClient: mockHttpClient,dishWasherDetailsMapper: mockMapper)
        
        expect(sut, toCompleteWith: .failure(DishwasherDetailRetriever.Error.invalidData), using: "testId")
        
    }
    
    //MARK: - assert helper 
    private func expect(_ sut: DishwasherDetailRetriever,
                        toCompleteWith expectedResult: DishwasherDetailsCompletion,
                         using  diswasherId:String,
                        file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for load completion")
        
        sut.fetchDishwasherDetails(using: diswasherId) { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedDishwashers), .success(expectedDishwashers)):
                XCTAssertEqual(receivedDishwashers, expectedDishwashers, file: file, line: line)
                
            case let (.failure(receivedError as NSError), .failure(expectedError as NSError)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)
                
            default:
                XCTFail("Expected result \(expectedResult), got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
  
        wait(for: [exp], timeout: 1.0)
    }
    
}
