//
//  DishwasherDetailMapperTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class DishwasherDetailsMapperTests: XCTestCase {
    
    func makeSUT() -> DefaultDishwasherDetailsMapper {
        DefaultDishwasherDetailsMapper()
    }
    
    func testSuccessfullyMapping() throws {
        let responseURL = try XCTUnwrap(Bundle(for: type(of: self)).url(forResource: "data2", withExtension: "json"))
        let data = try XCTUnwrap(Data(contentsOf: responseURL))
        let sut = makeSUT()
        
        let result:[DishwasherDetails] = try sut.map(data: data, urlResponse: .init())
        
        XCTAssertEqual(result.count, 10)
        
        // check first model in list
        XCTAssertEqual(result.first?.id, "3218074")
        XCTAssertEqual(result.first?.productInformation, "<p>Boasting plenty of convenient features within a simple-to-use design, the SMS24AW01G Freestanding Dishwasher from Bosch is sure to be a welcome helping hand in any kitchen.</p>\r\r<p><strong>dosageAssist</strong><br>\rThis useful feature makes sure your detergent is completely dissolved for a more effective wash by releasing it into a special tray on top of the basket and mixing it into the cycle.</p>\r\r<p><strong>ecoSilence</strong><br>\rBosch\'s revolutionary brushless motor has been developed to achieve maximum power with minimum energy loss. It\'s now quieter, faster, more energy efficient, durable and powerful.</p>\r\r<p><strong>Flexible loading</strong><br>\rThe SMS24AW01G offers total flexibility with its varioBasket system. The top basket can be adjusted between 3 height positions and both top and bottom drawers contain plenty of racking and 2 foldable cup shelves each.</p>\r\r<p><strong>Additional features:</strong><br>\r<ul>\r<li>Internal water protection</li>\r<li>Glass protection</li> \r<li>Self-cleaning filter</li>\r<li>Eco 50°C programme</li>\r<li>Quick 45 °C wash</li></ul></p>")
        
    }
    
    func testUnsuccessfullyMappingThrowsError() throws {
        let responseURL = try XCTUnwrap(Bundle(for: type(of: self)).url(forResource: "data2", withExtension: "json"))
        let data = try XCTUnwrap(Data(contentsOf: responseURL))
        let anyUrl = try XCTUnwrap(URL(string: "www.anyurl.som"))
        let invalidResponse = try XCTUnwrap(HTTPURLResponse(url: anyUrl, statusCode: 404, httpVersion:  nil, headerFields: nil))
        let sut = makeSUT()
        
        // invalid data
        do {
            let _:[DishwasherDetails] = try sut.map(data: Data(), urlResponse: .init())
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode )")
        } catch NetworkResponseMapperError.parsingError{
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.parsingError ) but got \(error)")
        }
        
        // invalid response
        do {
            let _:[DishwasherDetails] = try sut.map(data: data, urlResponse: invalidResponse)
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode )")
        } catch NetworkResponseMapperError.nonTwoHundredErrorCode {
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode ) but got \(error)")
        }
        
        
    }
}
