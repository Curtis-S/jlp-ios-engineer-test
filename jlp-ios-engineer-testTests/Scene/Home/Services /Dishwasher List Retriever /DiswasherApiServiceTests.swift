//
//  DiswasherApiService.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class DishwasherListRetrieverTests: XCTestCase {
    func makeSUT(dishWasherListMapper: DishwasherListMapper =  DishwasherListMapperSpy(),
                 httpClient: HttpClient = HttpClientSpy()
                 
    ) -> DishwasherListRetriever {
        DishwasherListRetriever(dishWasherListMapper: dishWasherListMapper,
                                httpClient: httpClient)
    }
    
    
    
    func testFetchDishwasherListCreatesCorrectRequest() {
        
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherListMapperSpy()
        
        let sut = makeSUT(dishWasherListMapper: mockMapper,httpClient: mockHttpClient)
        let expectation = expectation(description: "")
        sut.fetchDishwashers() { result in
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 1)
        
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockMapper.mapDataUrlResponseCallsCount,1)
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockHttpClient.dataForReceivedRequest?.url?.absoluteString, DishwasherApiEndpoint.list.url.absoluteString)
    }
    
    func testFetchDishwasherListReturnsWhatIsReturnedFromTheMapper()  {
        let mockDishWasher = Dishwasher.randomMock()
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherListMapperSpy()
        mockMapper.mapDataUrlResponseReturnValue = [mockDishWasher]
        
        let sut = makeSUT(dishWasherListMapper: mockMapper,httpClient: mockHttpClient)
        
        expect(sut, toCompleteWith: .success([mockDishWasher])) {
            
        }
        
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockMapper.mapDataUrlResponseCallsCount,1)
        XCTAssert(mockMapper.mapDataUrlResponseCalled)
        XCTAssertEqual(mockHttpClient.dataForReceivedRequest?.url?.absoluteString, DishwasherApiEndpoint.list.url.absoluteString)
        
    }
    
    func testFetchDishwasherFailsWithConnectivityErrorOnClientFail()  {
        let mockHttpClient = HttpClientSpy()
        mockHttpClient.dataForThrowableError = TestError.mock
        let sut = makeSUT(httpClient: mockHttpClient)
        
        expect(sut, toCompleteWith: .failure(DishwasherListRetriever.Error.connectivity)) {
            
        }
        
        XCTAssertEqual(mockHttpClient.dataForReceivedRequest?.url?.absoluteString, DishwasherApiEndpoint.list.url.absoluteString)
    }
    
    func testFetchDishwasherFailsWithDataErrorOnMapperFail()  {
        let mockHttpClient = HttpClientSpy()
        let mockMapper = DishwasherListMapperSpy()
        mockMapper.mapDataUrlResponseThrowableError = TestError.mock
        
        let sut = makeSUT(dishWasherListMapper: mockMapper,httpClient: mockHttpClient)
        
        expect(sut, toCompleteWith: .failure(DishwasherListRetriever.Error.invalidData)) {
            
        }
        
        XCTAssertEqual(mockHttpClient.dataForReceivedRequest?.url?.absoluteString, DishwasherApiEndpoint.list.url.absoluteString)
    }
    
    //MARK: - assert helper
    
    private func expect(_ sut: DishwasherListRetriever,
                        toCompleteWith expectedResult: DishwasherListRetrieverCompletion,
                        when action: () -> Void,
                        file: StaticString = #file, line: UInt = #line) {
        let exp = expectation(description: "Wait for load completion")
        
        sut.fetchDishwashers() { receivedResult in
            switch (receivedResult, expectedResult) {
            case let (.success(receivedDishwashers), .success(expectedDishwashers)):
                XCTAssertEqual(receivedDishwashers, expectedDishwashers, file: file, line: line)
                
            case let (.failure(receivedError as NSError), .failure(expectedError as NSError)):
                XCTAssertEqual(receivedError, expectedError, file: file, line: line)
                
            default:
                XCTFail("Expected result \(expectedResult), got \(receivedResult) instead", file: file, line: line)
            }
            
            exp.fulfill()
        }
        action()
        wait(for: [exp], timeout: 1.0)
    }
    
}


