//
//  DishwasherListMapperTests.swift
//  jlp-ios-engineer-testTests
//
//  Created by curtis scott on 11/02/2024.
//

import Foundation
import XCTest
@testable import jlp_ios_engineer_test

class DishwasherListMapperTests: XCTestCase {
    
    func makeSUT() -> DefaultDishwasherListMapper {
        DefaultDishwasherListMapper()
    }
    
    func testSuccessfullyMapping() throws {
        let responseURL = try XCTUnwrap(Bundle(for: type(of: self)).url(forResource: "data", withExtension: "json"))
        let data = try XCTUnwrap(Data(contentsOf: responseURL))
        let sut = makeSUT()
        
        let result:[Dishwasher] = try sut.map(data: data, urlResponse: .init())
        
        XCTAssertEqual(result.count, 24)
        
        // check first model in list
        
        XCTAssertEqual(result.first?.productId, "1955287")
        XCTAssertEqual(result.first?.adjustableRackingInfo, "YES")
        XCTAssertEqual(result.first?.alternateImageUrls, ["//johnlewis.scene7.com/is/image/JohnLewis/234378764alt1", "//johnlewis.scene7.com/is/image/JohnLewis/234378764alt10", "//johnlewis.scene7.com/is/image/JohnLewis/234378764alt2", "//johnlewis.scene7.com/is/image/JohnLewis/234378764alt3", "//johnlewis.scene7.com/is/image/JohnLewis/234378764alt9"])
        XCTAssertEqual(result.first?.childLock, "NO")
        XCTAssertEqual(result.first?.claimsText,"")
        XCTAssertEqual(result.first?.delayWash, "YES")
        XCTAssertEqual(result.first?.delicateWash, "NO")
        XCTAssertEqual(result.first?.dimensions, "H81.5 x W59.8 x D55cm")
        XCTAssertEqual(result.first?.dryingPerformance, "A")
        XCTAssertEqual(result.first?.dryingSystem, "Residual Heat")
        XCTAssertEqual(result.first?.guaranteeText, nil)
        XCTAssertEqual(result.first?.mainImageURL, "//johnlewis.scene7.com/is/image/JohnLewis/234378764?")
        XCTAssertEqual(result.first?.name, "Bosch Serie 2 SMV40C30GB Fully Integrated Dishwasher")
        XCTAssertEqual(result.first?.price, "379.00")
        XCTAssertEqual(result.first?.productInformation, nil)
    }
    
    func testUnsuccessfullyMappingThrowsError() throws {
        let responseURL = try XCTUnwrap(Bundle(for: type(of: self)).url(forResource: "data", withExtension: "json"))
        let data = try XCTUnwrap(Data(contentsOf: responseURL))
        let anyUrl = try XCTUnwrap(URL(string: "www.anyurl.som"))
        let invalidResponse = try XCTUnwrap(HTTPURLResponse(url: anyUrl, statusCode: 404, httpVersion:  nil, headerFields: nil))
        let sut = makeSUT()
        
        // invalid data
        do {
            let _:[Dishwasher] = try sut.map(data: Data(), urlResponse: .init())
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode )")
        } catch NetworkResponseMapperError.parsingError{
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.parsingError ) but got \(error)")
        }
        
        // invalid response
        do {
            let _:[Dishwasher] = try sut.map(data: data, urlResponse: invalidResponse)
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode )")
        } catch NetworkResponseMapperError.nonTwoHundredErrorCode {
            
        } catch {
            XCTFail("expected \( NetworkResponseMapperError.nonTwoHundredErrorCode ) but got \(error)")
        }
        
        
    }
}
